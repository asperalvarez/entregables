package com.techu.apitechu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "Hola Mundo desde API Tech U!";
    }

    @RequestMapping("/hello")
    public String greetings(@RequestParam(value = "name", defaultValue = "Mundo") String name){
         return String.format("Hola %s desde API Tech U!", name);
    }

    @RequestMapping("/suma")
    public String suma(@RequestParam(value = "var1", defaultValue = "0") int var1, @RequestParam(value = "var2", defaultValue = "0") int var2){
        int resultado = var1 + var2;
        return String.format("La suma de %1$d y %2$d es %3$d", var1, var2, resultado);

    }

}


