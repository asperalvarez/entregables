package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;


@SpringBootApplication
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);

		ApitechuApplication.productModels=ApitechuApplication.getTestData();
	}

	public static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<ProductModel>();

		productModels.add( new ProductModel( "1", "Producto 1", 10f));

		productModels.add( new ProductModel( "2", "Producto 2", 20.2f));

		productModels.add( new ProductModel( "3", "Producto 3", 30.2f));

		return productModels;
	}


}
