package com.techu.apitechu.controllers;
import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL ="/apitechu/v1";

    @GetMapping(APIBaseURL +"/products")
    public ArrayList<ProductModel> getProducts(){

        return ApitechuApplication.productModels;

    }

    @GetMapping(APIBaseURL +"/product/{id}")
    public ProductModel getProduct(@PathVariable String id){

     System.out.println("getProduct");
     System.out.println("el id es "+ id);

        ProductModel result = new ProductModel();

        for (ProductModel product: ApitechuApplication.productModels){

            if(product.getId().equals(id)){
                result= product;
            }
        }

        return result;

    }

    @PostMapping(APIBaseURL +"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct "+ newProduct.getId());
        System.out.println("El id de producto es "+ newProduct.getId());
        System.out.println("La desc del producto es "+ newProduct.getDesc());
        System.out.println("El price de producto es "+ newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseURL +"/products/{id}")
    public ProductModel updateProduct(@PathVariable String id,@RequestBody ProductModel product){
        System.out.println("udpateProduct "+ product.getId());
        System.out.println("El id de producto a actualizar en parametro URL es "+ id);
        System.out.println("El id de producto a actualizar es "+ product.getId());
        System.out.println("La desc del producto a actualizar es "+ product.getDesc());
        System.out.println("El price de producto a actualizar es "+ product.getPrice());

        boolean found = false;

        for (ProductModel prod: ApitechuApplication.productModels){

            if(prod.getId().equals(id)){
                prod.setDesc(product.getDesc());
                prod.setPrice(product.getPrice());
                found = true;
            }

        }

        if(!found){
            return new ProductModel();
        }else{
            return product;
        }

    }

    @DeleteMapping(APIBaseURL +"/product/{id}")
    public ProductModel deleteProduct(@PathVariable String id){

        ProductModel deletedProduct= new ProductModel();

        for (ProductModel prod: ApitechuApplication.productModels){

            if(prod.getId().equals(id)){
                deletedProduct=prod;

            }

        }

        ApitechuApplication.productModels.remove(deletedProduct);

        return deletedProduct;
    }


    @PatchMapping(APIBaseURL +"/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id){

        // Casos a probar: No enviar nada / Enviar Desc y Price / Enviar sólo Desc / Enviar sólo Price

        System.out.println("patchProduct");
        System.out.println("Id del producto a patchear en URL es " + id);
        System.out.println("Desc del producto a patchear es " + product.getDesc());
        System.out.println("Precio del producto a patchear es " + product.getPrice());

        ProductModel result = new ProductModel();

        for(ProductModel prod : ApitechuApplication.productModels) {
            if (prod.getId().equals(id))
                result = prod;
        }
        if (result.getId()==null)
        {
            System.out.println("ERROR: producto no encontrado");
        }
        else {
            if(product.getDesc() != null) {
                result.setDesc(product.getDesc());
                System.out.println("Desc actualizado");
            }
            if(product.getPrice() != 0.0f) {
                result.setPrice(product.getPrice());
                System.out.println("Price actualizado");
            }


        }
        return result;
    }

}
