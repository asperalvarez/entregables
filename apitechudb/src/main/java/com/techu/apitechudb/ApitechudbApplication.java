package com.techu.apitechudb;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels=ApitechudbApplication.getTestData();
	}

	public static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<ProductModel>();

		productModels.add( new ProductModel( "1", "Producto 1", 10f));

		productModels.add( new ProductModel( "2", "Producto 2", 20.2f));

		productModels.add( new ProductModel( "3", "Producto 3", 30.2f));

		return productModels;
	}

}
