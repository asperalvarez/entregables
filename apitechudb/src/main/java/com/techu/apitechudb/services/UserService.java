package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    /* findAll */
    public List<UserModel> findAll(String order ){
        System.out.println("<Service> findAll");
        if(order!=null && !order.isEmpty())
         return this.userRepository.findAll(Sort.by(Sort.Direction.ASC,order));
        else return this.userRepository.findAll();
    }

    /* findById */

    public Optional<UserModel> findById(String id){
        System.out.println("<Service> findById");
        return this.userRepository.findById(id);


    }

    /* add */

    public UserModel add(UserModel user){
        System.out.println("<Service> add");
        return this.userRepository.save(user);
    }

    /* update */

    public UserModel update (UserModel user){
        System.out.println("<Service> update");
        return this.userRepository.save(user);
    }

    /* delete */

    public boolean delete (String id){
        System.out.println("<Service> delete");

        Optional<UserModel> userToDelete = this.userRepository.findById(id);

        if(userToDelete.isPresent()){

            this.userRepository.deleteById(id);

            return true;
        }else{

            return false;

        }

    }
}
