package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurcharseModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurcharseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurcharseService {

    @Autowired
    PurcharseRepository purcharseRepository;
    @Autowired
    ProductRepository productReposotory;
    @Autowired
    UserRepository userRepository;

    public List<PurcharseModel> findAll(){

        return this.purcharseRepository.findAll();

    }

    public Integer add(PurcharseModel purcharse){

       System.out.println("<Purcharse Service> add");

        boolean existeUsuario= this.userRepository.findById(purcharse.getUserID()).isPresent();

        if(!existeUsuario){
            System.out.println("<Purcharse Service> No existe el usuario "+ existeUsuario);
           return -1;

        }else if(this.findById(purcharse.getId()).isPresent()){
            System.out.println("<Purcharse Service> El id de compra ya existe "+ purcharse.getId() );
            return -2;

        }else {

            System.out.println("<Purcharse Service> Comprobamos listado de productos " + purcharse.getItems());

            Iterator it = purcharse.getItems().keySet().iterator();
            float totalprice = 0;
            boolean productsExists = true;

            while (it.hasNext()) {

                String idProducto = (String) it.next();
                Integer cantidad = purcharse.getItems().get(idProducto);

                System.out.println("<Purcharse Service> Comprobamos si existe producto: " + idProducto);

                Optional productToBy = this.productReposotory.findById(idProducto);

                if (productToBy.isPresent()) {
                    System.out.println("<Purcharse Service> Producto: " + idProducto + " cantidad: " + cantidad);
                    totalprice += (cantidad * this.productReposotory.findById(idProducto).get().getPrice());
                    System.out.println("<Purcharse Service> Total: " + totalprice);
                } else {
                    System.out.println("<Purcharse Service> Error: no existe producto: " + idProducto);
                    productsExists = false;
                    break;

                }

            }

            if(!productsExists) {
                return -3;
            }else{
                purcharse.setAmount(totalprice);
                if(this.purcharseRepository.save(purcharse) instanceof PurcharseModel){
                    System.out.println("<Purcharse Service> Generada OK...");
                    return 0;
                }else{
                    System.out.println("<Purcharse Service> Error al guardar compra...");
                    return -4;
                }
            }


           }



    }

    public Optional<PurcharseModel> findById(String id){

        System.out.println("<Purcharse Service> findById");

        return this.purcharseRepository.findById(id);
    }

    public PurcharseModel update(PurcharseModel purcharse){

        System.out.println("<Purcharse Service> update");

        return this.purcharseRepository.save(purcharse);
    }

    public boolean delete(String id) {

        System.out.println("<Purcharse Service> delete");

        Optional<PurcharseModel> purchaseToDelete = this.findById(id);

        if (purchaseToDelete.isPresent()) {

            this.purcharseRepository.deleteById(id);

            return true;

        } else {

            return false;

        }
    }


}
