package com.techu.apitechudb.models;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.Objects;

@Document(collection = "purcharses")
public class PurcharseModel {

    String id;
    String userID;
    Float amount;
    Map<String,Integer> items;

    public PurcharseModel(){


    }

    public PurcharseModel(String id, String userID, Float amount, Map<String, Integer> items) {
        this.id = id;
        this.userID = userID;
        this.amount = amount;
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public Float getAmount() {
        return amount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public void setItems(Map<String, Integer> items) {
        this.items = items;
    }

    public Map<String, Integer> getItems() {
        return items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurcharseModel that = (PurcharseModel) o;
        return Objects.equals(id, that.id) && Objects.equals(userID, that.userID) && Objects.equals(amount, that.amount) && Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userID, amount, items);
    }

}
