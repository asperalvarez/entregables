package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value="order",required = false) String order){

        System.out.println("<Controller> getUsers");
        System.out.println("<Controller> Orden "+ order);
        return new ResponseEntity<>(this.userService.findAll(order), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){

        System.out.println("<Controller> getUserById");

        Optional<UserModel> userID= this.userService.findById(id);

        return new ResponseEntity<>(
                userID.isPresent()? userID.get(): "Usuario no encontrado",
                userID.isPresent()? HttpStatus.OK: HttpStatus.NOT_FOUND);

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){

        System.out.println("<Controller> addUser");
        return new ResponseEntity<>(this.userService.add(user),HttpStatus.CREATED);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){

        System.out.println("<Controller> addUser");

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser? "Usuario eliminado" : "Usuario no encontado",
                deletedUser?  HttpStatus.OK :  HttpStatus.NOT_FOUND);

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user , @PathVariable String id){

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){
            user.setId(id);
            this.userService.update(user);
        }

        return new ResponseEntity<>(user,
                userToUpdate.isPresent()? HttpStatus.OK:HttpStatus.NOT_FOUND);

    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<Object> patchUser(@RequestBody UserModel user , @PathVariable String id){

        Optional<UserModel> userToPath = this.userService.findById(id);

        if(userToPath.isPresent()){

            if(user.getAge()>0){
                userToPath.get().setAge(user.getAge());
            }

            if(!user.getName().isEmpty()){
                userToPath.get().setName(user.getName());
            }

            this.userService.update(userToPath.get());
        }

        return new ResponseEntity<>(
                userToPath.isPresent()? userToPath.get():"Usuario no encontrado",
                userToPath.isPresent()? HttpStatus.OK:HttpStatus.NOT_FOUND);

    }
}
