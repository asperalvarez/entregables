package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurcharseModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurcharseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/apitechu/v2")
public class PurcharseController {

    @Autowired
    PurcharseService purcharseService;


    @PostMapping("/purcharses")
    public ResponseEntity<String> addPurcharse(@RequestBody PurcharseModel purcharse) {

        System.out.println("<Purcharse Controller> addPurcharse");

        Integer result = this.purcharseService.add(purcharse);

        switch (result) {
            case 0:
                return new ResponseEntity<>("Compra creada satisfactoriamente", HttpStatus.OK);
            case -1:
                return new ResponseEntity<>("Usuario no existe", HttpStatus.CONFLICT);
            case -2:
                return new ResponseEntity<>("Identificador de compra ya existente", HttpStatus.CONFLICT);
            case -3:
                return new ResponseEntity<>("Alguno de los productos no existen", HttpStatus.CONFLICT);
            default:
                return new ResponseEntity<>("ERROR: ha habido un error al generar la compra", HttpStatus.CONFLICT);

        }

    }


    @GetMapping("/purcharses")
    public ResponseEntity<List<PurcharseModel>> getPurchases() {

        System.out.println("<Purcharse Controller> getPurchases");
        return new ResponseEntity<>(this.purcharseService.findAll(), HttpStatus.OK);
    }

    }

