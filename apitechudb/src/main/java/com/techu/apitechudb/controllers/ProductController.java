package com.techu.apitechudb.controllers;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

        @Autowired
        ProductService productService;

        @GetMapping("/products")
        @CrossOrigin(origins = "*", methods = {RequestMethod.GET})
        public ResponseEntity<List<ProductModel>> getProducts(){

            System.out.println("getProducts");
            return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);

        }



        @PostMapping("/products")
        public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){

        System.out.println("addProduct");
        System.out.println("El id del producto que se va a crear es "+product.getId());
        System.out.println("La descripción del producto que se va a crear es "+product.getDesc());
        System.out.println("El precio del producto que se va a crear es "+product.getPrice());

        return new ResponseEntity<>(this.productService.add(product),HttpStatus.CREATED);

        }

        @GetMapping("products/{id}")
        public ResponseEntity<Object> getProductById(@PathVariable String id){

            System.out.println("getProductById");
            System.out.println("El id del producto que vamos a obtener es "+id);

            Optional<ProductModel> result = this.productService.findById(id);

            return new ResponseEntity<>(
                    result.isPresent()? result.get() : "Producto no encontrado"
                    , result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);

        }

        @PutMapping("products/{id}")
        public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){

            System.out.println("updateProduct");
            System.out.println("El id del producto que vamos a actualizar es "+id);
            System.out.println("El id (body) del producto que vamos a actualizar es "+product.getId());
            System.out.println("La descripción del producto que se va a actualizar  es "+product.getDesc());
            System.out.println("El precio del producto que se va a actualizar es "+product.getPrice());

            Optional<ProductModel> productToUpdate = this.productService.findById(id);

            if(productToUpdate.isPresent()){

                System.out.println("Producto para actualizar encontrad, actualizando");

                this.productService.update(product);
            }

            return new ResponseEntity<>(product,
                    productToUpdate.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
        }


        @DeleteMapping("products/{id}")
        public ResponseEntity<String> deleteProduct(@PathVariable String id){

            System.out.println("deleteProduct");
            System.out.println("El id del producto que vamos a borrar es "+id);

            Optional<ProductModel> productToDelete = this.productService.findById(id);

            if(productToDelete.isPresent()){

                System.out.println("Producto encontrado, eliminando");

                this.productService.delete(id);
            }

            return new ResponseEntity<>(
                    productToDelete.isPresent()? "Producto encontrado" : "Producto NO encontrado",
                    productToDelete.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
        }

}
